import MongoConnection from './infrastructure/database/MongoConnection';
import ExpressServer from './infrastructure/utils/ExpressServer';
require('dotenv').config();

const app = new ExpressServer();
const PORT = parseInt(process.env.PORT || '3000');
const MONGO_URL_CONNECTION = String(process.env.MONGO_URL_CONNECTION || '');

const mongoDB = new MongoConnection();
mongoDB.connect(MONGO_URL_CONNECTION);


app.run(PORT);
