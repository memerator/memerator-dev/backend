import mongoose from 'mongoose';

export default class MongoConnection {
  connect(stringConnection: string): void {
    try {
      mongoose.connect(stringConnection, { useNewUrlParser: true, useUnifiedTopology: true }, () => {
        console.log('MongoDB connected!')
      });
    }
    catch {
      console.log('There was a problem connecting with MongoDB')
    }
  }
}
