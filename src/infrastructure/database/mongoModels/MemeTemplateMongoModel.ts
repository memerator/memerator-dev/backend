import { Schema, model } from "mongoose";
import mongoosePaginate from "mongoose-paginate-v2";

const memeTemplateSchema = new Schema({
  imageUrl: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  episode: {
    type: String,
    required: false
  }
})
memeTemplateSchema.plugin(mongoosePaginate);
export default model('MemeTemplate', memeTemplateSchema);
