import MemeTemplate from "../../domain/memeTemplate/entities/MemeTemplate";
import MemeTemplateMongoModel from "./mongoModels/MemeTemplateMongoModel";
export default class MemeTemplateDAO {
  constructor() { }
  async findByQuery(query: string, pageNumber: number, pageLimit: number) {
    const options = {
      page: pageNumber,
      limit: pageLimit,
      customLabels: { docs: 'data', meta: 'meta' }
    };
    return await MemeTemplateMongoModel.paginate(
      {
        description: {
          $regex: query,
          $options: '$i'
        }
      },
      options
    );
  }
  async getMemeTemplates(pageNumber: number, pageLimit: number) {
    const options = {
      page: pageNumber,
      limit: pageLimit,
      customLabels: { docs: 'data', meta: 'meta' }
    };

    const memeTemplates = await MemeTemplateMongoModel.paginate({}, options);
    return memeTemplates;
  }
  async createMemeTemplate(memeTemplate: MemeTemplate) {
    const newMemeTemplate: any = await new MemeTemplateMongoModel({
      description: memeTemplate.description,
      imageUrl: memeTemplate.imageUrl,
      episode: memeTemplate.episode
    });
    return await newMemeTemplate.save();
  }
}
