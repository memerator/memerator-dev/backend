import express, { Express } from "express";
import cors from "cors";
import MemeTemplateController from "../controllers/MemeTemplateController";
import MultimediaController from "../controllers/MultimediaController";
import swaggerUi from "swagger-ui-express";
import swaggerJsdoc from "swagger-jsdoc";
import swaggerConfig from "./swaggerConfig.json"

export default class ExpressServer {
    private _server: Express;
    constructor() {
        this._server = express()
        this.configExpressServer();
    }
    private configExpressServer() {
        this._server.use(express.json())
        this._server.use(cors());
        this._server.use(
            '/',
            (new MemeTemplateController()).router,
            (new MultimediaController()).router
        );
        this._server.use(
            '/api-docs',
            swaggerUi.serve,
            swaggerUi.setup(swaggerJsdoc(swaggerConfig))
        )
        this._server.get('/', (req, res) => res.redirect('/api-docs'))

    }
    run(port: number): void {
        this._server.listen(port, () => {
            console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
        });
    }
}