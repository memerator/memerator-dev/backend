import MemeTemplateService from "../../domain/memeTemplate/services/MemeTemplateService";
import { Request, Response, Router } from "express";
import MemeTemplate from "src/domain/memeTemplate/entities/MemeTemplate";

/**
 * @swagger
 * components:
 *   schemas:
 *     MemeTemplate:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *         imageUrl:
 *           type: string
 *           description: The url where the image is hosted
 *         episode:
 *           type: string
 *           description: Just the season and chapter for example S01E01
 *         description:
 *           type: string
 *           description: An array of strings where describe everything in the image
 */

export default class MemeTemplateController {
    private _memeTemplateService: MemeTemplateService;
    private _router: Router;

    constructor() {
        this._router = Router();
        this._memeTemplateService = new MemeTemplateService();
        this.config();
    }

    private async getAllMemeTemplates(req: Request, res: Response) {
        try {
            const { page, limit } = req.query;
            const pageNumber: number = Number.isNaN(parseInt(page + '')) ? 1 : parseInt(page + '')
            const pageLimit: number = Number.isNaN(parseInt(limit + '')) ? 10 : parseInt(limit + '')
            const memeTemplates = await this._memeTemplateService.getAllMemeTemplates(pageNumber, pageLimit);
            return res.status(200).send(memeTemplates);
        } catch (error) {
            return res.status(500).send(error);
        }
    }
    private async createMemeTemplate(req: Request, res: Response) {
        try {
            const { description, imageUrl, episode } = req.body;
            const imageData: MemeTemplate = {
                description: String(description),
                imageUrl: String(imageUrl),
                episode: String(episode)
            };
            const image = await this._memeTemplateService.createMemeTemplate(imageData);
            return res.status(200).send(image);
        } catch (error) {
            return res.status(500).send(error);
        }
    }

    private async getMemeTemplatesBySearch(req: Request, res: Response) {
        try {
            const { page, limit, query } = req.query;
            const querySearch: string = (query ? query + '' : '');
            const pageNumber: number = Number.isNaN(parseInt(page + '')) ? 1 : parseInt(page + '')
            const pageLimit: number = Number.isNaN(parseInt(limit + '')) ? 10 : parseInt(limit + '')
            const imagesResponse = await this._memeTemplateService.searchMemeTemplates(querySearch, pageNumber, pageLimit);
            return res.status(200).send(imagesResponse);
        } catch (error) {
            return res.status(500).send(error);
        }
    }



    private config(): void {

        /**
         * @swagger
         * /api/meme-templates:
         *  get:
         *    tags:
         *      - MemeTemplate
         *    description: Use to request all memeTemplates
         *    responses:
         *      '200':
         *         description: A successful response
         *    parameters:
         *      - name: page
         *        description: The number of page (pagination) to get the memeTemplates
         *        in: query
         *        type: string
         *      - name: limit
         *        description: The count of docs (memeTemplate) in the page
         *        in: query
         *        type: string
         */
        this._router.get('/api/meme-templates', this.getAllMemeTemplates.bind(this));

        /**
         * @swagger
         * /api/meme-templates/search:
         *  get:
         *    tags:
         *      - MemeTemplate
         *    description: Use to request meme-templates by a query
         *    responses:
         *      '200':
         *        description: A successful response
         *    parameters:
         *      - name: q
         *        description: Strings to match with meme-templates description
         *        in: query
         *        type: string
         *      - name: page
         *        description: The number of page (pagination) to get the meme-templates
         *        in: query
         *        type: string
         *      - name: limit
         *        description: The count of docs (meme-templates) in the page
         *        in: query
         *        type: string
         */
        this._router.get('/api/meme-templates/search', this.getMemeTemplatesBySearch.bind(this))

        /**
         * @swagger
         * /api/meme-templates:
         *  post:
         *    tags:
         *      - MemeTemplate
         *    description: Create a new image object
         *    requestBody:
         *      description: An image object
         *      content:
         *        application/json:
         *          schema:
         *            $ref: "#/components/schemas/MemeTemplate"
         *    responses:
         *      '200':
         *        description: A successful response
         */
        this._router.post('/api/meme-templates', this.createMemeTemplate.bind(this))
    }

    public get router(): Router {
        return this._router;
    }


}