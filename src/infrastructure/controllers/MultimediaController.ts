
import { Request, Response, Router } from "express";
import IMultimediaService from "../../domain/multimedia/interfaces/IMultimediaService";
import MultimediaService from "../../domain/multimedia/services/MultimediaService";
import multer from "multer";
import path from "path";
import fs from "fs-extra";
require('dotenv').config();
const cloudinary = require('cloudinary');
const cloudConf = {
    cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET,
}
cloudinary.config(cloudConf)

const storage = multer.diskStorage({
    destination: 'uploads/',
    filename: (req, file, callback) => {
        callback(null, new Date().getTime() + path.extname(file.originalname));
    }
})

/**
 * @swagger
 * components:
 *   schemas:
 *     Multimedia:
 *       type: object
 *       properties:
 *         fileUrl:
 *           type: string
 *           description: The url where the multimedia file is stored
 */

export default class MultimediaController {
    private _multimediaService: IMultimediaService;
    private _router: Router;

    constructor() {
        this._router = Router();
        this._multimediaService = new MultimediaService();
        this.config();
    }

    private async storeImage(req: Request, res: Response) {
        try {
            const result = await cloudinary.v2.uploader.upload(req.file.path)
            await fs.unlink(req.file.path);
            return res.status(201).send({ fileUrl: result.secure_url })
        } catch (error) {
            return res.status(500).send(error);
        }
    }

    private config(): void {

        /**
         * @swagger
         * /api/multimedia/image:
         *  post:
         *      tags:
         *          - Multimedia
         *      summary: Store a multimedia file to the CDN
         *      description: Store a multimedia file to the CDN.
         *      responses:
         *          '201':
         *              description: File stored successfully
         *              content:
         *                  application/json:
         *                      schema:
         *                          $ref: "#/components/schemas/Multimedia"
         *      requestBody:
         *          content:
         *              multipart/form-data:
         *                  schema:
         *                      type: object
         *                      properties:
         *                          image:
         *                              type: string
         *                              format: binary
         */
        this._router.post(
            '/api/multimedia/image',
            multer({ storage }).single('image'),
            this.storeImage.bind(this),
        );
    }

    public get router(): Router {
        return this._router;
    }


}