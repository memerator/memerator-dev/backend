import MemeTemplateDAO from "../../../infrastructure/database/MemeTemplateDAO";
import MemeTemplate from "../entities/MemeTemplate";

export default class MemeTemplateService {
    private _memeTemplateDAO: MemeTemplateDAO;
    constructor() {
        this._memeTemplateDAO = new MemeTemplateDAO();
    }
    async searchMemeTemplates(query: string, pageNumber: number, pageLimit: number) {
        return await this._memeTemplateDAO.findByQuery(query, pageNumber, pageLimit);
    }
    async getAllMemeTemplates(pageNumber: number, pageLimit: number) {
        return await this._memeTemplateDAO.getMemeTemplates(pageNumber, pageLimit);
    }
    async createMemeTemplate(memeTemplate: MemeTemplate) {
        return await this._memeTemplateDAO.createMemeTemplate(memeTemplate);
    }
}
