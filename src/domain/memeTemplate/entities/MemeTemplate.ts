export default interface MemeTemplate {
    imageUrl: string;
    episode: string;
    description: string;
}